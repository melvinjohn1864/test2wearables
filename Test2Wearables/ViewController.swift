//
//  ViewController.swift
//  Test2Wearables
//
//  Created by Melvin John on 2019-11-07.
//  Copyright © 2019 Melvin John. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {
    
    let USERNAME = "melvinjohn1864@gmail.com"
    let PASSWORD = "Melvin@1864"
    
    let DEVICE_ID = "25003d000447363333343435"
    var myPhoton : ParticleDevice?
    
    var timeInterval: Double!
    
    var interval: TimeInterval!
    
    var timer = Timer()
    
    var countTimer = 0
    
    @IBOutlet weak var slowDownLabel: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    
    
    @IBOutlet weak var btnMonitor: UIButton!
    
    
    @IBOutlet weak var labelTimeElapsed: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        timeInterval = 1.0
        
        interval = 0
        
        interval = interval + timeInterval
        
        self.labelTimeElapsed.text = String(countTimer)
        
        // 1. Initialize the SDK
        ParticleCloud.init()
        
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    
    
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
                
            }
            
        }
    }
    
    
    

    @IBAction func startMonitoring(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: interval, target: self, selector: Selector(("increaseTimer")), userInfo: nil, repeats: true)
    }
    
    @objc func increaseTimer(){
        countTimer = countTimer + 1
        self.labelTimeElapsed.text = String(countTimer)
        
        if (countTimer < 4){
            self.showFaceAnimationOnParticle(second: countTimer)
        }else if(countTimer >= 4 && countTimer < 8){
            self.showFaceAnimationOnParticle(second: countTimer)
        }else if(countTimer >= 8 && countTimer < 12){
            self.showFaceAnimationOnParticle(second: countTimer)
        }else if(countTimer >= 12 && countTimer < 16){
            self.showFaceAnimationOnParticle(second: countTimer)
        }else if(countTimer >= 16 && countTimer < 20){
            self.showFaceAnimationOnParticle(second: countTimer)
        }else if(countTimer >= 20){
            self.showFaceAnimationOnParticle(second: countTimer)
        }
        
    }
    
    func showFaceAnimationOnParticle(second: Int) {
        
        let time = String(second)
        let parameters = [time]
        var task = myPhoton!.callFunction("animate", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to face animate")
            }
            else {
                print("Error when telling Particle to face animate")
            }
        }
        
    }
    
    //Min value of timer is zero
    //Max value of timer is 10
    //Time changes by 1 seconds
   
    @IBAction func sliderMoved(_ sender: UISlider) {
        let currentValue = Int(sender.value)
        
        interval = interval! + Double(currentValue)
        
        slowDownLabel.text = "Time Slows down by: \(currentValue)"
        
        if(countTimer >= 0){
            countTimer = countTimer - currentValue
        }else {
            countTimer = countTimer + currentValue
        }
    }
    
}

