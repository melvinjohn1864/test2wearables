// This #include statement was automatically added by the Particle IDE.
#include <InternetButton.h>
#include <math.h>

InternetButton button = InternetButton();

int animateTime;

void setup() {
    
    button.begin();
    
    Particle.function("animate", showFaceAnimation);

}

void loop() {

}

int showFaceAnimation(String cmd){
    RGB.control(true);
    animateTime = cmd.toInt();
    
    if (animateTime < 4){
        RGB.brightness(255);
        button.ledOff(2);
        button.ledOff(10);
        button.ledOn(1,0,255,0);
        button.ledOn(11,0,255,0);
        button.ledOn(3,0,255,0);
        button.ledOn(4,0,255,0);
        button.ledOn(5,0,255,0);
        button.ledOn(6,0,255,0);
        button.ledOn(7,0,255,0);
        button.ledOn(8,0,255,0);
        button.ledOn(9,0,255,0);
        delay(1000);
    }else if (animateTime >= 4 && animateTime < 8){
        RGB.brightness(255);
        button.ledOn(1,0,255,0);
        button.ledOn(11,0,255,0);
        button.ledOff(3);
        button.ledOn(4,0,255,0);
        button.ledOn(5,0,255,0);
        button.ledOn(6,0,255,0);
        button.ledOn(7,0,255,0);
        button.ledOn(8,0,255,0);
        button.ledOff(9);
        delay(1000);
    }else if (animateTime >= 8 && animateTime < 12){
        RGB.brightness(128);
        button.ledOn(1,0,255,0);
        button.ledOn(11,0,255,0);
        button.ledOff(4);
        button.ledOn(5,0,255,0);
        button.ledOn(6,0,255,0);
        button.ledOn(7,0,255,0);
        button.ledOff(8);
        delay(1000);
    }else if (animateTime >= 12 && animateTime < 16){
        RGB.brightness(128);
        button.ledOn(1,0,255,0);
        button.ledOn(11,0,255,0);
        button.ledOff(5);
        button.ledOn(6,0,255,0);
        button.ledOff(7);
        delay(1000);
    }else if (animateTime >= 16 && animateTime < 20){
        RGB.brightness(64);
        button.ledOn(1,0,255,0);
        button.ledOn(11,0,255,0);
        button.ledOff(6);
        delay(1000);
    }else if (animateTime >= 20){
        RGB.control(false);
        button.ledOff(1);
        button.ledOff(11);
        button.ledOn(2,255,0,0);
        button.ledOn(10,255,0,0);
        button.ledOn(5,255,0,0);
        button.ledOn(7,255,0,0);
        delay(1000);
        
    }
     return 1;
}